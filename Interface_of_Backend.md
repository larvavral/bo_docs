# Interface of Backend server with the other parts

### 1. How to get the price to display in the screen

Rate of a symbol is get from **Price Engine** module. Currently, 10 rates is generated per second, we can config it in configure file of **Price Engine**.
Subscribe `price_engine_message` for new rate coming event.

```
> SUBSCRIBE price_engine_message
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "price_engine_message"
3) (integer) 1
1) "message"
2) "price_engine_message"
3) "btcusd"
```

Then get data from key `price_engine_data_btcusd` for *fair value*.

```
> GET price_engine_data_btcusd
"{"ask_price":{"bitfinex":{"ccy":"","new":"0.0000","org":"0.0000"},"bitflyer1":{"ccy":"jpy","new":"3583.8211","org":"394098.0000"},"bitflyer2":{"ccy":"jpy","new":"3563.8876","org":"391906.0000"},"bithumb":{"ccy":"krw","new":"3546.1606","org":"3990000.0000"},"coinone":{"ccy":"","new":"0.0000","org":"0.0000"},"gemini":{"ccy":"","new":"0.0000","org":"0.0000"},"kraken":{"ccy":"","new":"0.0000","org":"0.0000"},"okcoin":{"ccy":"","new":"0.0000","org":"0.0000"},"quoine":{"ccy":"","new":"0.0000","org":"0.0000"}},"available":{"bitfinex":"0","bitflyer1":"1","bitflyer2":"1","bithumb":"1","coinone":"0","gemini":"0","kraken":"0","okcoin":"0","quoine":"0"},"bid_price":{"bitfinex":{"ccy":"","new":"0.0000","org":"0.0000"},"bitflyer1":{"ccy":"jpy","new":"3579.2924","org":"393600.0000"},"bitflyer2":{"ccy":"jpy","new":"3563.1692","org":"391827.0000"},"bithumb":{"ccy":"krw","new":"3534.6067","org":"3977000.0000"},"coinone":{"ccy":"","new":"0.0000","org":"0.0000"},"gemini":{"ccy":"","new":"0.0000","org":"0.0000"},"kraken":{"ccy":"","new":"0.0000","org":"0.0000"},"okcoin":{"ccy":"","new":"0.0000","org":"0.0000"},"quoine":{"ccy":"","new":"0.0000","org":"0.0000"}},"fair_value":"3581.5567","mid_price":{"bitfinex":{"ccy":"","new":"0.0000","org":"0.0000"},"bitflyer1":{"ccy":"jpy","new":"3581.5567","org":"393849.0000"},"bitflyer2":{"ccy":"jpy","new":"3563.5284","org":"391866.5000"},"bithumb":{"ccy":"krw","new":"3540.3837","org":"3983500.0000"},"coinone":{"ccy":"","new":"0.0000","org":"0.0000"},"gemini":{"ccy":"","new":"0.0000","org":"0.0000"},"kraken":{"ccy":"","new":"0.0000","org":"0.0000"},"okcoin":{"ccy":"","new":"0.0000","org":"0.0000"},"quoine":{"ccy":"","new":"0.0000","org":"0.0000"}},"mov_avr":"3581.7089","std_avr_ratio":"0.001443","timestamp":"1549858295849"}"
```

Extract `fair_value` from json. And if necessary, you can check `timestamp` field also to know whether this is old rate or not.

### 2. Round status

Subscribe `round_status` channel, message will be responsed by the following format:

```
{
    "round_number": 12,
    "status": "start",
    "start_price": 3577.58,
    "end_price": 0,
    "odd_up": 0,
    "odd_down": 0
}
```

- `round_number`: unique number represent for a round.
- `status`: can be 
    - `start` - take the `start_price` and allow user order up/down
    - `close` - close betting and wait for the result
    - `end` - take the `end_price` and calculate `odd_up`, `odd_down`
- `start_price`: this field will be filled when `status` is "start"
- `end_price`: this field will be filled when `status` is "end". It used to judge result of a round is up or down.
- `odd_up`, `odd_down`: how many lot user gain with the result.

    For ex: 
    ```
    odd_up = 1.45, odd_down = 0 
    => It means price go up. And with each up lot betting, user receive 1.45 lots back.
    ```

### 3. Behavior of backend server when receive `status`

When receive `status`
- start: backend server display `start_price` and allow user vote up/down
- close: backend server send to redis information about how many lots for up/down (already count the commission). Maybe another channel, such as, channel `round_info`, message
    ```
    {
        "total_lots_up": 12,
        "total_lots_down": 3
    }
    ```
    please feel free to discuss.
- end: receive `end_price`, `odd_up`, `odd_down`. Based on these values, add or minus from customer's account balance, and display the result to UI.