# BO System

This document describe an overview of the BO system, feel free to discuss and give any solution for a better design.

![BO System Overview](bo_system.png)

As you can see in the above image, system include 6 modules:
- **Load balancer**: When number of requests from client increase, we need to scale more backend server and handle it. So we need a load balancer to devide requests from client to backend servers. At first version, we will not use it, just one backend server is OK.
- **Backend**: Main component of system. It has responsibility for
    - Manage account information, authenticate.
    - Manage account balance and call *Wallet API* when user want to deposit or withdraw.
    - Receive bet order from client and send to redis for odd calculation.
- **Admin page**: A client which is developed to help operation team manage system. Some example tasks: mange price engine parameter, stop round by manually, send notification to all customer, etc.
- **Odd calculation**: We use this module to synchronize behavior of all backend server. The idea is 
    - When round start, notify to all backend module to allow customer order their bets. And use price at this time as mock for judge up/down result
    - After 30s, notiy to all backend module to stop receiving customer orders.
    - After 30s, take the price and calculate odd then send back to the backend server to pay to customer (add or minus balance of customer account)

    Note that each round is 1 minute, we can config it from config file or admin page.
- **Gateway**: Get the orderbook from other trading platform like bitfinext, bithumb, coinone, etc. And convert rate between fiats from oanda, xe also.
- **Price Engine**: This module uses data from gateway to create a fair value that we think it's the best suitable for us.
- **Redis**: Communication gate between modules.
- **MySQL**: Store account information, execution history, round information. We can use these information for analyst later.

### Why we need *Load balancer* and *Odd calculation* modules?

To serve many customers and orders at the same time, we need to scale *backend server* in future as number of customers increase. So we need a load balancer to devide orders to these *backend servers* reasonably.
When use many *backend servers*, each server will have different number of lot bet for up/down (different orders), so how to we collect all data in *backend servers* for calculation odd. That is reason to build *odd calculation* module. Btw, we still need a mechanism to sync the time between different *backend servers*, so we can also use *odd calculation* module.